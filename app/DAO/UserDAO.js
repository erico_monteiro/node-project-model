module.exports = class UserDAO {

    constructor(connection)     {
        this._connection = connection;
    }

    list() {
        
        return new Promise((resolve, reject) => {
            this._connection.query(queryListALL, (err, users) => {
                if (err) reject(err);
                resolve(users);
            })
        });

    }

}

const queryCreateTable = 
`CREATE TABLE User (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NULL,
    email VARCHAR(45) NULL,
    password VARCHAR(45) NULL,
    PRIMARY KEY (id));`;

const queryListALL = 'SELECT * FROM User';
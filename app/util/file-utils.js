const fileUtils = {};
const fs = require('fs');
const ENCODE ='utf8';

fileUtils.fileToObject = (fileName) => {
    return JSON.parse(fs.readFileSync(fileName, ENCODE));
}

module.exports = fileUtils;
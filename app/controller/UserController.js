const UserDAO = require('../DAO/UserDAO');

module.exports = function(app) {

    app.get('/user', (req, res) => {
        new UserDAO(req.connection)
        .list()
        .then(users => res.json(users))
        .catch(err => res.status(500).send({error: err}));
    });

}
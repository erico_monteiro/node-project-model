const mysql = require('mysql');
const fileUtils = require('../util/file-utils');
const dbSettiongs = fileUtils.fileToObject('properties/db-connection.json');

const pool = mysql.createPool({
    connectionLimit: dbSettiongs.connectionLimit,
    host: dbSettiongs.host,
    user: dbSettiongs.user,
    password: dbSettiongs.password,
    database: dbSettiongs.database
});

console.log('pool => criado');

pool.on('release', () => console.log('pool => conexão retornada'));

process.on("SIGINT", () => 
    pool.end(err => {
        if (err) return console.log(err);
        console.log('pool => fechado');
        process.exit(0);
    })
);

module.exports = pool;
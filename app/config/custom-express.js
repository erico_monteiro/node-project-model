const express = require('express')
, app = express()
, pool = require('./pool-factory')
, connectionMiddleware = require('./connection-middleware');

const consign = require('consign');
const bodyParser = require('body-parser');

module.exports = function() {

    app.use(connectionMiddleware(pool));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));

    app.use((err, req, res, next) => {
        console.error(err.stack);
        res.status(500).json({error: err.toString()});
    });

    consign()
    .include('./app/controller')
    .into(app);

    return app;
}
